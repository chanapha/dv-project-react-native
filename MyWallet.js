import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  ListView
} from "react-native";
import { connect } from "react-redux";
import { Button, SwipeAction } from "@ant-design/react-native";

class MyWallet extends Component {
  render() {
    return (
      <View style={{ width: "100%", height: "100%", alignItems: "center" }}>
        {this.props.wallets.map(wallet => {
          return (
            <TouchableOpacity
              style={stylesSheet.wallet}
              onPress={() => this.props.navigate(wallet.id, "/WalletPage")}
            >
              <Text style={{ fontSize: 24 }}>{wallet.name}</Text>
              <Text style={{ fontSize: 24 }}>
                {Number.parseFloat(wallet.value).toFixed(2)}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }
}

const stylesSheet = StyleSheet.create({
  wallet: {
    height: 80,
    width: '80%',
    paddingHorizontal: 20,
    margin: 8,
    borderWidth: 6,
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    borderColor: 'violet',
    backgroundColor: '#e6ccff'

  }
});

const mapStateToProps = state => {
  console.log("wallets", state.wallets.wallets);
  return {
    user: state.user,
    wallets: state.wallets.wallets
  };
};

export default connect(mapStateToProps)(MyWallet);
