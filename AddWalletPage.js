import React, { Component } from "react";
import { View, Image,Alert } from "react-native";
import { connect } from 'react-redux'
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  Footer,
  FooterTab,
  Icon,
  Picker
} from "native-base";


class AddWalletPage extends Component {
    state = {
        walletName: '',
        initialMoney: ''
    }

    addNewWallet = () => {
        const wallet = {
            id: `${this.props.nextId}`,
            name: this.state.walletName === '' ? 'My Wallet' : this.state.walletName,
            value: this.state.initialMoney === '' ? 0.00 : parseFloat(this.state.initialMoney),
            transactions: []
        }
        this.props.addWallet(wallet)
        Alert.alert('Add New Wallet', 'Successful')
        this.props.history.replace('/Profile')
    }
  render() {
    return (
      <Container>
        <Header>
          <Button transparent onPress={this.addNewWallet} >
            <Text>Succeed</Text>
          </Button>
          <Body style={{ width: "70%" }}>
            <Title style={{ marginLeft: "25%" }}>Add Wallet</Title>
          </Body>
          <Button
            transparent
            onPress={() => this.props.history.replace("/WalletPage")} >
            <Text>Cancel</Text>
          </Button>
        </Header>

        <Content>
        <Text style={{ color: 'violet', fontSize: 22, paddingTop: 30, textAlign: 'center', marginBottom: 50,marginTop: 50}}>Add your money wallet </Text>

          <Item>
            {/* <Icon active name="home" /> */}
            
            <Input 
            value={this.state.walletName}
            onChangeText={value => this.setState({ walletName: value })}
            placeholder="Add your wallet name" />
          </Item>

          <Item>
            
            <Input 
            value={this.state.initialMoney}
            onChangeText={value => { this.setState({ initialMoney: value }) }}
            placeholder="Add your balance " />
            {/* <Icon active name="swap" /> */}
          </Item>
        </Content>
      </Container>
    );
  }
}

    const mapDispatchToProps = (dispatch) => {
        return {
            addWallet: (wallet) => {
                dispatch({
                    type: 'ADD_WALLET',
                    payload: wallet
                })
            }
        }
    }

    const mapStateToProps = (state) => {
        return {
          nextId: state.wallets.wallets.length,
          currentWallet: state.wallets.currentWallet
        }
    }

    export default connect(mapStateToProps, mapDispatchToProps)(AddWalletPage)
