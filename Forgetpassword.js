import React, {Component} from 'react';
import { View } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Content, Form, Item, Input, Label, Button, Text } from 'native-base';

export default class Login extends Component {
  render() {
    return (
        <Container>
        <Header>
            <Left />
                <Body>
                    <Title>Forget Password</Title>
                </Body>
            <Right />
        </Header>
        <Content style={{ backgroundColor: '#e6ccff' }}>
            <Form>
                <Item stackedLabel last>
                    <Label>Email</Label>
                    <Input placeholder='example@moneygo.com'  />
                </Item>
            </Form>
            <Text />
            <Button block  >
                <Text>Send reset password email</Text>
            </Button>
            <Text style={{ textAlign: 'center' }}>{'\nOr'}</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <Button primary transparent onPress = {() => this.props.history.replace('/Register')}   >
                    <Text>Don't have an account yet? Register.</Text>
                </Button>
            </View>
        </Content>
    </Container>
    );
  }
}