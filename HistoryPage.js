import React, { Component } from "react";
import { View, Image } from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  Footer,
  FooterTab,
  Icon,
  Picker
} from "native-base";
import { connect } from "react-redux";

class HistoryPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected: undefined
    };
  }
  onValueChange(value) {
    this.setState({
      selected: value
    });
  }

  render() {
    return (
      <Container >
        <Header>
            <Button transparent onPress={() => this.props.history.replace("/WalletPage")}>
              <Text>Back</Text>
            </Button>
          <Body style={{width: '70%'}}>
            <Title style={{marginLeft: '25%'}}>History</Title>
          </Body>
            <Button transparent onPress={() => this.props.history.replace("/WalletPage")}>
              <Text>Cancel</Text>
            </Button>       
        </Header>


        <Content>
        <Text style={{ color: 'violet', fontSize: 22, paddingTop: 30, textAlign: 'center'}}>History </Text>
          {this.props.transactions.map(transaction => {
            if (transaction.value > 0) {
              return (
                <Button
                  bordered
                  success
                  style={{ marginTop: 20, width: "100%" }}
                >
                  <Text>
                    {transaction.category}
                    {"+" + transaction.value}
                  </Text>
                  <Text>{transaction.timestamp.toString().substring(0, 24)}</Text>
                </Button>
              );
            } else {
              return (
                <Button
                  bordered
                  danger
                  style={{ marginTop: 20, width: "100%" }}
                >
                  <Text>
                    {transaction.category}
                    {" " + transaction.value}
                  </Text>
                  <Text>{transaction.timestamp.toString().substring(0, 24)}</Text>
                </Button>
              );
            }
          })}
        </Content>

        <Footer>
          <FooterTab>
            <Button
              active
              onPress={() => this.props.history.replace("/WalletPage")}
            >
              <Text>Wallet</Text>
            </Button>
            <Button
              active
              onPress={() => this.props.history.replace("/IncomeCategory")}
            >
              <Text>Income</Text>
            </Button>
            <Button
              onPress={() => this.props.history.replace("/PurchaseCategory")}
            >
              <Text>Purchase</Text>
            </Button>
            <Button onPress={() => this.props.history.replace("/HistoryPage")}>
              <Text>History</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  console.log(
    "trans",
    state.wallets.wallets[state.wallets.currentWallet].transactions
  );
  return {
    balance: state.wallets.wallets[state.wallets.currentWallet].value,
    transactions:
      state.wallets.wallets[state.wallets.currentWallet].transactions
  };
};

export default connect(mapStateToProps)(HistoryPage);
