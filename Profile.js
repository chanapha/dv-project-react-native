import React, { Component } from "react";
import { View, Image, TouchableOpacity, ScrollView } from "react-native";
import MyWallet from "./MyWallet";
import _ from 'lodash';
import { connect } from "react-redux";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  List,
  ListItem,
  Icon,
  SwipeRow
} from "native-base";

class Profile extends Component {

  state = {
    image:
      "https://png.pngtree.com/element_pic/17/08/10/5722b2c827fd69f0b38ac454cdbbe9e6.jpg"
  };

  navigate(index, path) {
    this.props.setCurrentWallet(Number.parseInt(index));
    this.props.history.replace(path);
  }

  render() {
    return (
      <Container>
        <ScrollView>
        <Header>
          <Body style={{width: '70%'}}>
            <Title style={{marginLeft: '25%'}}>MoneyGo</Title>
          </Body>
            <Button transparent onPress={() => this.props.history.replace("/")}>
              <Text>Logout</Text>
            </Button>       
        </Header>

          <Content scrollEnabled={false}>
          {/* <Image
            source={{ uri: this.state.image }}
            style={{ width: 250, height: 250, marginBottom: 10, alignSelf: 'center' }}
          /> */}
           <Image style={{ width: 200, height: 200,marginTop: 1, alignSelf: 'center' }} source={require('./logo.png')} />
           <Text style={{ color: 'violet', fontSize: 15, paddingTop: 10, textAlign: 'center'}}>Your balance </Text>
            <Item regular style={{ width: "100%", marginBottom: 30 }}>
              <Input placeholder="0.00" disabled value={'' + this.props.total}/>
            </Item>

            <Button
              block
              success
              onPress={() => this.props.history.replace("/AddWalletPage")}
            >
              {/* <Icon style={{ fontSize: 30 }} name="add-circle" /> */}
              <Text>+ Add your wallet</Text>
            </Button>

            <SwipeRow
              leftOpenValue={75}
              rightOpenValue={-75}
              body={
                <View>
                  <TouchableOpacity
                    onPress={() => this.props.history.replace("/WalletPage")}
                  >
                    <MyWallet navigate={this.navigate.bind(this)} />
                  </TouchableOpacity>
                </View>
              }
              right={
                <Button danger onPress={() => alert("Trash")}>
                  <Icon active name="trash" />
                </Button>
              }
            />
          </Content>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    wallets: state.wallets,
    total: _.reduce(_.map(state.wallets.wallets, wallet => wallet.value), (x, y) => x + y)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setCurrentWallet: (index) => {
      dispatch({ type: 'SET_CURRENT_WALLET', payload: index});
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Profile);
