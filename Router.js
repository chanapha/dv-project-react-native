import React, {Component} from 'react'
import {Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Login from './Login'
import Register from './Register'
import Profile from './Profile'
import Forgetpassword from './Forgetpassword'
import IncomeCategory from './IncomeCategory'
import PurchaseCategory from './PurchaseCategory'
import WalletPage from './WalletPage'
import HistoryPage from './HistoryPage'
import AddWalletPage from './AddWalletPage'

import { store, history } from './Reducer/Store'
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux'

class Router extends Component{
    render(){
        return(
        <Provider store={store}>
             <ConnectedRouter history={history}>
           
                <Switch>
                    <Route exact path= "/" component= {Login}/>
                    <Route exact path= "/Forgetpassword" component= {Forgetpassword}/>
                    <Route exact path= "/Register" component= {Register}/>
                    <Route exact path= "/Profile" component= {Profile}/>
                    <Route exact path= "/IncomeCategory" component= {IncomeCategory}/>
                    <Route exact path= "/PurchaseCategory" component= {PurchaseCategory}/>
                    <Route exact path= "/WalletPage" component= {WalletPage}/>
                    <Route exact path= "/HistoryPage" component= {HistoryPage}/>
                    <Route exact path= "/AddWalletPage" component= {AddWalletPage}/>
                </Switch>
           
            </ConnectedRouter>
        </Provider>
        )
    }
}

export default Router