import React, { Component } from "react";
import { View, Image, Alert } from "react-native";
import { connect } from 'react-redux'
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text
} from "native-base";
import axios from 'axios'

class Login extends Component {
      state = {
        email: 'janey@gmail.com',
        password: '0000',
        isLoading: false
    }
    login = async () => {
        try {
            this.setState({ isLoading: true })
            const res = await axios.post('https://zenon.onthewifi.com/moneyGo/users/login', {
                email: this.state.email,
                password: this.state.password
            })
            const user = res.data.user
            await this.props.userLogin(user)
            this.setState({ isLoading: false })
            await this.props.history.push('/Profile')
        } catch (error) {
            this.setState({ isLoading: false })
            const err = error.response.data.errors ? error.response.data.errors : error
            console.log('login response', error.response.data.errors);
            let errorMessage = ''
            if (err.email) errorMessage += '- Email: ' + err.email + '\n'
            if (err.password) errorMessage += '- Password: ' + err.password + '\n'
            Alert.alert('Incorrect Login', errorMessage)
        }
    }

    signup = () => {
      this.props.history.push('/Register')
    }

    forgetpass = () => {
      this.props.history.push('/Forgetpassword')
    }

    static navigationOptions = {
      header: null
    };

    constructor(props) {
      super(props);
      this.state = {
        image:
          "https://png.pngtree.com/element_pic/17/08/10/5722b2c827fd69f0b38ac454cdbbe9e6.jpg"
          
      };
    }

  render() {
    const { email, password } = this.state;
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Login</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          {/* <Image
            source={{ uri: this.state.image }}
            style={{ width: 150, height: 150, marginTop: 1, alignSelf: 'center' }}
          /> */}

        <Image style={{ width: 200, height: 200,marginTop: 1, alignSelf: 'center' }} source={require('./logo.png')} />

          <Form>
            <Item stackedLabel>
              <Label>Email</Label>
              <Input placeholder="example@moneygo.com" value={email} onChangeText={email => { this.setState({ email })} } autoCorrect={false} autoCapitalize='none' />
            </Item>
            <Item stackedLabel last>
              <Label>Password</Label>
              <Input placeholder="Super secure password" value={password} onChangeText={password => { this.setState({ password })}} autoCorrect={false} autoCapitalize='none' secureTextEntry />
            </Item>
          </Form>
          <Text />
          <Button block onPress={this.login}  >
            <Text>Login</Text>
          </Button>
          <Button style={{ marginTop: 20 }} block onPress={() => this.props.history.replace("/Profile")}  >
            <Text>Guest Login</Text>
          </Button>
          <Text style={{ textAlign: "center" }}>{"\nOr"}</Text>
          <View style={{ flexDirection: "row", justifyContent: "center" }}>

            <Button
              dark
              transparent onPress={this.signup}>
              <Text>Register?</Text>
            </Button>

            <Button
              danger
              transparent
              onPress={this.forgetpass}>
              <Text>Forget Password?</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      userLogin: (user) => {
          dispatch({
              type: 'USER_LOGIN',
              user: user
          })
      }
  }
}

export default connect(null, mapDispatchToProps)(Login)