import React, { Component } from "react";
import { View, Image } from "react-native";
import { connect } from 'react-redux'
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  Footer,
  FooterTab,
  Icon,
  Picker
} from "native-base";

class IncomeCategory extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected: undefined, 
      Incomevalue: '',
      Incomecategory: 'Salary'
    };
  }
  onValueChange(value) {
    this.setState({
      Incomecategory: value
    });
  }

  addTransaction(){
    const { Incomecategory, Incomevalue} = this.state;
    const transaction = {
      value: Number.parseFloat(Incomevalue),
      category: Incomecategory,
      timestamp: new Date()
    }
    this.props.addTransaction(transaction);
    this.props.history.replace("/WalletPage");
  }

  render() {
    return (
      <Container >
        <Header>
            <Button transparent onPress={this.addTransaction.bind(this)} >
              <Text>Success</Text>
            </Button>
          <Body style={{width: '70%'}}>
            <Title style={{marginLeft: '25%'}}>Income</Title>
          </Body>
            <Button transparent onPress={() => this.props.history.replace("/WalletPage")}>
              <Text>Cancel</Text>
            </Button>       
        </Header>
        
        <Content>
        <Text style={{ color: 'violet', fontSize: 22, paddingTop: 30, textAlign: 'center'}}>Income </Text>
        <Item regular style={{width:"80%", marginLeft: 20, marginTop: 80, marginBottom: 50}}>
            <Input placeholder='0.00' value={this.state.Incomevalue} onChangeText={value => this.setState({ Incomevalue: value })} />
        </Item>
        <Text style={{ color: 'violet', fontSize: 16, paddingTop: 10, textAlign: 'center'}}>Select your category of income </Text>
        <Form>
            <Picker value={this.state.Incomecategory}  onChange={value => this.setState({ Incomecategory: value })}
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              style={{ width: undefined }}
              selectedValue={this.state.Incomecategory}
              onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label="Sarary" value="Salary" />
              <Picker.Item label="Bonus" value="Bonus" />
              <Picker.Item label="Freelance" value="Freelance" />
              <Picker.Item label="Gift" value="Gift" />
              <Picker.Item label="Lottery" value="Lottery" />
              <Picker.Item label="Other" value="Other" />
            </Picker>
          </Form>

        </Content>

        <Footer>
          <FooterTab>
          <Button active onPress={() => this.props.history.replace("/WalletPage")}>
              <Text>Wallet</Text>
            </Button>
            <Button active onPress={() => this.props.history.replace("/IncomeCategory")}>
              <Text>Income</Text>
            </Button>
            <Button onPress={() => this.props.history.replace("/PurchaseCategory")}>
              <Text>Purchase</Text>
            </Button>
            <Button onPress={() => this.props.history.replace("/HistoryPage")}>
              <Text>History</Text>
            </Button>
          </FooterTab>
        </Footer>
       
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      addTransaction: (transaction) => {
        dispatch({ type: 'ADD_TRANSACTION', payload: transaction });
      }
  }
}

export default connect(null, mapDispatchToProps)(IncomeCategory)