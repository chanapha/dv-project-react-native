const INITIAL_STATE = {
  wallets: [
    {
      id: "0",
      name: "Wallet 1",
      value: 1000.0,
      transactions: [
        {
          category: "Bonus",
          value: 1100.0,
          timestamp: new Date()
        },
        {
          category: "Food",
          value: -100.0,
          timestamp: new Date()
        }
      ]
    }
  ],
  currentWallet: 0
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "ADD_WALLET":
      return addWallet(state, action);
    case "DELETE_WALLET":
      const deleted = state.splice(
        state.indexOf(item => {
          return action.payload._id === item._id;
        }),
        1
      );
      return deleted;
    case "ADD_TRANSACTION":
      return addTransaction(state, action);
    case "SET_CURRENT_WALLET":
      return { ...state, currentWallet: action.payload };
    default:
      return state;
  }
};

const addWallet = (state, action) => {
  const wallet = action.payload;
  return { ...state, wallets: [...state.wallets, wallet] };
};

const addTransaction = (state, action) => {
  const index = state.currentWallet;
  const transaction = action.payload;
  let newState = state;
  newState.wallets[index].transactions = [...newState.wallets[index].transactions, transaction];
  newState.wallets[index].value = newState.wallets[index].value + transaction.value
  return newState;
};
