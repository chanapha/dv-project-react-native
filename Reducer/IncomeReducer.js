export default (state = {
    incomevalue: null,
    incomecategory: null
}, action) => {
    console.log(action);
    switch (action.type) {
        case 'INCOME_VALUE':
            return {
                ...state, incomevalue: action.payload
            }
        case 'INCOME_CATEGORY':
            return {
                ...state, incomecategory: action.payload
            }
        default:
            return state
    }
}