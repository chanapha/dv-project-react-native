import { createMemoryHistory } from 'history'
import { applyMiddleware, createStore, combineReducers, compose } from 'redux'
import { routerMiddleware, connectRouter } from 'connected-react-router'
import logger from 'redux-logger'
import UserReducer from './UserReducer';
import WalletReducer from './WalletReducer';


const reducers = (history) => combineReducers({
    user: UserReducer,
    wallets: WalletReducer,
    router: connectRouter(history)
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const history = createMemoryHistory()

export const store = createStore(
    reducers(history), 
    composeEnhancers(
        applyMiddleware(
            routerMiddleware(history),
            logger
        )
    )
)
