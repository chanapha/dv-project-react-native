import React, { Component } from "react";
import { View, Image } from "react-native";
import { connect } from "react-redux";

import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  Footer,
  FooterTab,
  Icon
} from "native-base";

class WalletPage extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Button
            transparent
            onPress={() => this.props.history.replace("/Profile")}
          >
            <Text>Back</Text>
          </Button>
          <Body style={{ width: "70%" }}>
            <Title style={{ marginLeft: "25%" }}>Wallet</Title>
          </Body>
        </Header>

        <Content>

        <Image style={{ width: 200, height: 200,marginTop: 1, alignSelf: 'center' }} source={require('./logo.png')} />

          <Text style={{ color: 'violet', fontSize: 22, paddingTop: 1, textAlign: 'center'}}>Your balance </Text>
          <Item
            regular
            style={{ width: "100%", marginBottom: 10, marginTop: 20 }}
          >
            <Input placeholder={"" + this.props.balance} disabled />
          </Item>
          <Text style={{ color: 'violet', fontSize: 22, paddingTop: 1, textAlign: 'center'}}>Transactions </Text>
          {this.props.transactions.map(transaction => {
            if (transaction.value > 0) {
              return (
                <Button
                  bordered
                  success
                  style={{ marginTop: 20, width: "100%" }}
                >
                  <Text>
                    {transaction.category}
                    {" +" + transaction.value}
                  </Text>
                </Button>
              );
            } else {
              return (
                <Button
                  bordered
                  danger
                  style={{ marginTop: 20, width: "100%" }}
                >
                  <Text>
                    {transaction.category}
                    {" " + transaction.value}
                  </Text>
                </Button>
              );
            }
          })}
        </Content>

        <Footer>
          <FooterTab>
            <Button
              active
              onPress={() => this.props.history.replace("/WalletPage")}
            >
              <Text>Wallet</Text>
            </Button>
            <Button
              active
              onPress={() => this.props.history.replace("/IncomeCategory")}
            >
              <Text>Income</Text>
            </Button>
            <Button
              onPress={() => this.props.history.replace("/PurchaseCategory")}
            >
              <Text>Purchase</Text>
            </Button>
            <Button onPress={() => this.props.history.replace("/HistoryPage")}>
              <Text>History</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    balance: state.wallets.wallets[state.wallets.currentWallet].value,
    transactions:
      state.wallets.wallets[state.wallets.currentWallet].transactions
  };
};

export default connect(mapStateToProps)(WalletPage);
