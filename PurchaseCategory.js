import React, { Component } from "react";
import { View, Image } from "react-native";
import { connect } from 'react-redux'
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Content,
  Form,
  Item,
  Input,
  Button,
  Text,
  Footer,
  FooterTab,
  Icon,
  Picker
} from "native-base";

class PurchaseCategory extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected: undefined, 
      Purchasevalue: '',
      Purchasecategory: 'Food'
    };
  }
  onValueChange(value) {
    this.setState({
      Purchasecategory: value
    });
  }

  addTransaction(){
    const { Purchasecategory, Purchasevalue} = this.state;
    const transaction = {
      value: - Number.parseFloat(Purchasevalue),
      category: Purchasecategory,
      timestamp: new Date()
    }
    this.props.addTransaction(transaction);
    this.props.history.replace("/WalletPage");
  }

  render() {
    return (
      <Container >
        <Header>
            <Button transparent onPress={this.addTransaction.bind(this)}>
              <Text>Succeed</Text>
            </Button>
          <Body style={{width: '70%'}}>
            <Title style={{marginLeft: '25%'}}>Purchase</Title>
          </Body>
            <Button transparent onPress={() => this.props.history.replace("/WalletPage")}>
              <Text>Cancel</Text>
            </Button>       
        </Header>


        <Content>
        <Text style={{ color: 'violet', fontSize: 22, paddingTop: 30, textAlign: 'center'}}>Purchase </Text>
        <Item regular style={{width:"80%", marginLeft: 20, marginTop: 80, marginBottom: 50}}>
            <Input placeholder='0.00' value={this.state.Purchasevalue} onChangeText={value => this.setState({ Purchasevalue: value })} />
        </Item>

        <Text style={{ color: 'violet', fontSize: 16, paddingTop: 10, textAlign: 'center'}}>Select your category of purchase </Text>
        <Form>
            <Picker value={this.state.Purchasecategory}  onChange={value => this.setState({ Purchasecategory: value })}
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              style={{ width: undefined }}
              selectedValue={this.state.Purchasecategory}
              onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label="Food" value="Food" />
              <Picker.Item label="Shopping" value="Shopping" />
              <Picker.Item label="Travel" value="Travel" />
              <Picker.Item label="Online" value="Online" />
              <Picker.Item label="Cosmatic" value="Cosmatic" />
              <Picker.Item label="Work" value="Work" />
              <Picker.Item label="Other" value="Other" />
            </Picker>
          </Form>
        </Content>



        <Footer>
          <FooterTab>
          <Button active onPress={() => this.props.history.replace("/WalletPage")}>
              <Text>Wallet</Text>
            </Button>
            <Button active onPress={() => this.props.history.replace("/IncomeCategory")}>
              <Text>Income</Text>
            </Button>
            <Button onPress={() => this.props.history.replace("/PurchaseCategory")}>
              <Text>Purchase</Text>
            </Button>
            <Button onPress={() => this.props.history.replace("/HistoryPage")} >
              <Text>History</Text>
            </Button>
          </FooterTab>
        </Footer>




       
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
      addTransaction: (transaction) => {
        dispatch({ type: 'ADD_TRANSACTION', payload: transaction });
      }
  }
}

export default connect(null, mapDispatchToProps)(PurchaseCategory)