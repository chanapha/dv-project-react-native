import firebase from 'firebase';
import 'firebase/firestore';

const config = {
    apiKey: "AIzaSyBZjDeLy-itvCZbx_aHNGPYsmNrFcVW6jU",
    authDomain: "dv-workshop-98da3.firebaseapp.com",
    databaseURL: "https://dv-workshop-98da3.firebaseio.com",
    projectId: "dv-workshop-98da3",
    storageBucket: "dv-workshop-98da3.appspot.com",
    messagingSenderId: "272131282866"
  };

firebase.initializeApp(config);
 
export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const database = firebase.database();
