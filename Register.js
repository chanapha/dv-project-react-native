import React, { Component } from "react";
import { View, Alert } from "react-native";
import {
    Container,
    Header,
    Left,
    Body,
    Right,
    Title,
    Content,
    Form,
    Item,
    Input,
    Label,
    Button,
    Text
} from "native-base";
import axios from "axios";

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
        email: "",
        password: "",
        cPassword: "",
        name: "",
        lastname: ""
        };
    }

    userSignUp = async () => {
        if (
        this.state.email === "" ||
        this.state.password === "" ||
        this.state.name === "" ||
        this.state.lastname === ""
        ) {
        Alert.alert(
            "Incorrect Input",
            "Please do not leave input fields blanked"
        );
        } else {
        if (this.state.password !== this.state.cPassword) {
            await Alert.alert(
            "Incorrect Input",
            "Confirmed password is not matched."
            );
        } else {
            try {
            await axios.post(
                "https://zenon.onthewifi.com/moneyGo/users/register",
                {
                email: this.state.email,
                password: this.state.password,
                firstName: this.state.name,
                lastName: this.state.lastname
                }
            );
             Alert.alert("Successed", "Signing up was successful");
             this.props.history.replace("/");
            } catch (error) {
              console.log(JSON.stringify(error))
            // console.log("signup res", error.response.data.errors);
            }
        }
        }
    };

  render() {
        const {
        email,
        password,
        confirmPassword,
        firstName,
        lastName
        } = this.state;
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Register</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Form>
            <Item stackedLabel>
              <Label>First Name</Label>
              <Input
                placeholder="FirstName"
                clear
                value={this.state.name}
                onChangeText={value => {
                  this.setState({ name: value });
                }}
              />
            </Item>
            <Item stackedLabel>
              <Label>Last Name</Label>
              <Input
                placeholder="LastName"
                clear
                value={this.state.lastname}
                onChangeText={value => {
                  this.setState({ lastname: value });
                }}
              />
            </Item>
            <Text />
            <Item stackedLabel>
              <Label>Email</Label>
              <Input
                placeholder="example@moneygo.com"
                clear
                value={this.state.email}
                onChangeText={value => this.setState({ email: value })}
              />
            </Item>
            <Text />
            <Item stackedLabel>
              <Label>Password</Label>
              <Input
                placeholder="Super secure password"
                clear
                type="password"
                value={this.state.password}
                onChangeText={value => {
                  this.setState({ password: value });
                }}
              />
            </Item>
            <Item stackedLabel last>
              <Label>Confirm Password</Label>
              <Input
                placeholder="Confirm password"
                clear
                type="password"
                value={this.state.cPassword}
                onChangeText={value => {
                  this.setState({ cPassword: value });
                }}
              />
            </Item>
          </Form>
          <Text />
          <Button block onPress={this.userSignUp}>
            <Text>Register</Text>
          </Button>
          <Text style={{ textAlign: "center" }}>{"\nOr\n"}</Text>
          <Button dark full onPress={() => this.props.history.replace("/")}>
            <Text>Already have an account?</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default Register;
